# Fuzzy Matching

This lets you search within a haystack for a needle of "gkwdtshw" which might result in "gtk_widget_show".

## Building

```sh
make
```

## Testing

```sh
./test-fuzzy gkwdtshw
```

## More information

More information on this can be found on [my blog](http://hergert.me/blog/2013/09/13/fuzzy-searching.html).
