#include "fuzzy.h"

#include <gio/gio.h>

gint
main (gint   argc,
      gchar *argv[])
{
   GFileInputStream *file_stream;
   GDataInputStream *data_stream;
   gint64 begin;
   gint64 end;
   GFile *file;
   Fuzzy *fuzzy;
   gchar *line;

   fuzzy = fuzzy_new_with_free_func(FALSE, g_free);
   file = g_file_new_for_path("input.txt");
   file_stream = g_file_read(file, NULL, NULL);
   data_stream = g_data_input_stream_new(G_INPUT_STREAM(file_stream));

   fuzzy_begin_bulk_insert(fuzzy);
   while ((line = g_data_input_stream_read_line(data_stream, NULL, NULL, NULL))) {
      fuzzy_insert(fuzzy, line, line);
   }
   fuzzy_end_bulk_insert(fuzzy);

   if (argc > 1) {
      FuzzyMatch *match;
      guint i;

      for (i = 1; i < argc; i++) {
         GArray *matches;
         guint j;

         g_printerr("Searching for \"%s\"\n", argv[i]);

         begin = g_get_monotonic_time();
         matches = fuzzy_match(fuzzy, argv[i], 0);
         end = g_get_monotonic_time();

         for (j = 0; j < matches->len; j++) {
            match = &g_array_index(matches, FuzzyMatch, j);
            g_print("  %0.4f : %s\n", match->score, match->key);
            g_assert_cmpstr(match->key, ==, match->value);
         }

         g_print("Completed query in: %ld msec.\n",
                 (glong)((end - begin) / 1000L));

         g_array_unref(matches);
      }

   }

   fuzzy_free(fuzzy);
   g_clear_object(&file_stream);
   g_clear_object(&data_stream);
   g_clear_object(&file);

   return 0;
}
