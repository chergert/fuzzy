all: test-fuzzy

test-fuzzy: fuzzy.c fuzzy.h main.c
	$(CC) -ggdb -o $@ -Wall -Werror $(shell pkg-config --cflags --libs gio-2.0) -Wl,-no-undefined main.c fuzzy.c

clean:
	rm -f test-fuzzy
